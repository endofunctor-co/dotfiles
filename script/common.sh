#!/usr/bin/env bash

# Colors
ESC_SEQ="\x1b["
COL_RESET=$ESC_SEQ"39;49;00m"
COL_GREY=$ESC_SEQ"30;01m"
COL_RED=$ESC_SEQ"31;01m"
COL_GREEN=$ESC_SEQ"32;01m"
COL_YELLOW=$ESC_SEQ"33;01m"
COL_BLUE=$ESC_SEQ"34;01m"

function info() {
  echo -e "$COL_BLUE[info]$COL_RESET $1"
}

function status() {
  echo -e "$COL_GREY[...]$COL_RESET $1"
}

function ok() {
  echo -e "$COL_GREEN[ok]$COL_RESET $1"
}

function installing() {
  echo -e "$COL_YELLOW[installing]$COL_RESET $1..."
}

function running() {
  echo -e "$COL_YELLOW[running]$COL_RESET $1..."
}

function action() {
  echo -e "$COL_YELLOW[action]$COL_RESET\n ⇒ $1..."
}

function warn() {
  echo -e "$COL_YELLOW[warning]$COL_RESET $1"
}

function error() {
  echo -e "$COL_RED[error]$COL_RESET $1"
}

function user() {
  echo -e "$COL_BLUE[??]$COL_RESET $1"
}

function link_file () {
  local src=$1 dst=$2
  local overwrite= backup= skip=
  local action=

  if [ -f "$dst" -o -d "$dst" -o -L "$dst" ]
  then
    if [ "$overwrite_all" == "false" ] && [ "$backup_all" == "false" ] && [ "$skip_all" == "false" ]
    then
      local currentSrc="$(readlink $dst)"

      if [ "$currentSrc" == "$src" ]
      then
        skip=true;
      else
        user "File already exists: $dst ($(basename "$src")), what do you want to do?\n\
        [s]kip, [S]kip all, [o]verwrite, [O]verwrite all, [b]ackup, [B]ackup all?"
        read -n 1 action

        case "$action" in
          o )
            overwrite=true;;
          O )
            overwrite_all=true;;
          b )
            backup=true;;
          B )
            backup_all=true;;
          s )
            skip=true;;
          S )
            skip_all=true;;
          * )
            ;;
        esac
      fi
    fi

    overwrite=${overwrite:-$overwrite_all}
    backup=${backup:-$backup_all}
    skip=${skip:-$skip_all}

    if [ "$overwrite" == "true" ]
    then
      rm -rf "$dst"
      ok "removed $dst"
    fi

    if [ "$backup" == "true" ]
    then
      mv "$dst" "${dst}.backup"
      ok "moved $dst to ${dst}.backup"
    fi

    if [ "$skip" == "true" ]
    then
      info "skipped $src"
    fi
  fi

  if [ "$skip" != "true" ]  # "false" or empty
  then
    parent=$(dirname "$2")
    if [ ! -d "$parent" ]
    then
      mkdir -p "$parent"
    fi
    ln -s "$1" "$2"
    ok "Linked $1 to $2"
  fi
}
